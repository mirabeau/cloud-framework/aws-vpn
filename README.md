AWS VPN
=======
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create VPN Gateway and VPN connections for VPC including Customer Gateways.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
* aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
create_changeset      : True
debug                 : False
cloudformation_tags   : {}
tag_prefix            : "mcf"

aws_vpn_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"
  amazon_side_asn : "" # Leave empty for auto generated
  propagate_routes:
    public  : False
    private : True
    database: True
  vpns: {}
  cgws: {}
```
### _Example configuration_
```yaml
aws_vpn_params:
  propagate_routes:
    public  : False
    private : True
    database: False
  vpns:
    MyCompany1:
      static_routes_only: False
    MyCompany2:
      static_routes_only: True
      tunnel_inside_cidrs:
        - 169.254.21.150/30
        - 169.254.22.170/30
  cgws:
    MyCompany1:
      gateway_ip: 35.123.123.123
      asn: 65100
    MyCompany2:
      gateway_ip: 35.321.321.321
      asn: 65200
```

Example Playbooks
----------------
Rollout VPN in tst environment
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
    aws_vpn_params:
      vpns:
        ACompany:
          tunnel_inside_cidrs:
            - 169.254.20.30/30
            - 169.254.20.34/30
        BCompany:
          static_routes_only: 'true'
        CCompany: {}
      cgws:
        ACompany:
          gateway_ip: 35.123.123.123
          asn: 65100
  roles:
    - aws-vpc
    - aws-vpn
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
